package shpp.com.ua.testdb.entyties;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import javax.validation.constraints.NotEmpty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import shpp.com.ua.testdb.enams.StorageUnit;

@Getter
@AllArgsConstructor
public class Bill {

  @JsonProperty(value = "amount")
  private BigDecimal amount;
  @JsonProperty(value = "storage_unit")
  @NotEmpty
  private StorageUnit storageUnit;

}
