package shpp.com.ua.testdb.entyties;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import shpp.com.ua.testdb.enams.IncomeItem;
import shpp.com.ua.testdb.enams.StorageUnit;

@Getter
@ToString
@Document("test_incomes")
@NoArgsConstructor
public class Income {

  @Id
  private String id;
  @JsonProperty(value = "account_name")
  private String accountName;
  @JsonProperty(value = "income_item")
  private IncomeItem incomeItem;
  @JsonProperty(value = "amount")
  private BigDecimal amount;
  @JsonProperty(value = "storage_unit")
  private StorageUnit storageUnit;

  public Income setId(String id) {
    this.id = id;
    return this;
  }

  public Income setAccountName(String accountName) {
    this.accountName = accountName;
    return this;
  }

  public Income setIncomeItem(IncomeItem incomeItem) {
    this.incomeItem = incomeItem;
    return this;
  }

  public Income setAmount(BigDecimal amount) {
    this.amount = amount;
    return this;
  }

  public Income setStorageUnit(StorageUnit storageUnit) {
    this.storageUnit = storageUnit;
    return this;
  }
}
