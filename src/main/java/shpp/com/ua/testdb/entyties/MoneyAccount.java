package shpp.com.ua.testdb.entyties;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.time.LocalDateTime;
import java.util.List;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;
import org.hibernate.validator.constraints.UniqueElements;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@ToString
@Document("test_bill")
@NoArgsConstructor
public class MoneyAccount {

  @Id
  private String id;
  @UniqueElements
  @JsonProperty(value = "name")
  private String name;
  @JsonProperty(value = "bills")
  private List<Bill> bills;
  @JsonProperty(value = "create_time")
  private LocalDateTime createTime;
  @JsonProperty(value = "current_time")
  private LocalDateTime currentTime;

  public MoneyAccount(String name, List<Bill> bills) {
    this.name = name;
    this.bills = bills;
    this.createTime = LocalDateTime.now();
    this.currentTime = this.createTime;
  }
}
