package shpp.com.ua.testdb.entyties;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.math.BigDecimal;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import shpp.com.ua.testdb.enams.CostItem;
import shpp.com.ua.testdb.enams.StorageUnit;

@Getter
@ToString
@Document("test_cost")
@NoArgsConstructor
public class Cost {

  @Id
  private String id;
  @JsonProperty(value = "account_name")
  private String accountName;
  @JsonProperty(value = "cost_item")
  private CostItem costItem;
  @JsonProperty(value = "amount")
  private BigDecimal amount;
  @JsonProperty(value = "storage_unit")
  private StorageUnit storageUnit;

  public Cost setAccountName(String accountName) {
    this.accountName = accountName;
    return this;
  }

  public Cost setCostItem(CostItem costItem) {
    this.costItem = costItem;
    return this;
  }

  public Cost setAmount(BigDecimal amount) {
    this.amount = amount;
    return this;
  }

  public Cost setStorageUnit(StorageUnit storageUnit) {
    this.storageUnit = storageUnit;
    return this;
  }
}
