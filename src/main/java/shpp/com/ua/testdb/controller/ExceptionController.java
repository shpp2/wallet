package shpp.com.ua.testdb.controller;

import org.springframework.dao.IncorrectResultSizeDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import shpp.com.ua.testdb.dto.ResponseMessage;

@ControllerAdvice
public class ExceptionController {

    @ResponseBody
    // Annotation that indicates a method return value should be bound to the web response body.
    @ExceptionHandler(IncorrectResultSizeDataAccessException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    ResponseMessage accountNotCreatedInDBHandler(IncorrectResultSizeDataAccessException ex) {
        return new ResponseMessage(HttpStatus.BAD_REQUEST, ex.getMessage());
    }
}
