package shpp.com.ua.testdb.controller;

import io.swagger.v3.oas.annotations.Operation;
import java.util.List;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import shpp.com.ua.testdb.dto.DtoAccount;
import shpp.com.ua.testdb.dto.DtoCost;
import shpp.com.ua.testdb.dto.DtoIncome;
import shpp.com.ua.testdb.dto.ResponseMessage;
import shpp.com.ua.testdb.entyties.Cost;
import shpp.com.ua.testdb.entyties.Income;
import shpp.com.ua.testdb.entyties.MoneyAccount;
import shpp.com.ua.testdb.servise.AccountService;
import shpp.com.ua.testdb.servise.BillService;

@RestController
@Slf4j
@RequestMapping("/account")
@RequiredArgsConstructor
public class BillsController {

  private final AccountService accountService;
  private final BillService billService;

  @GetMapping()
  @Operation(description = "provide an active account in the database")
  public ResponseEntity<List<MoneyAccount>> getAllAccount() {
    return ResponseEntity.ok(accountService.getMoneyAccount());
  }

  @GetMapping("/costs")
  @Operation
  public ResponseEntity<List<Cost>> getAllCosts() {
    return ResponseEntity.ok(billService.getAllCosts());
  }

  @GetMapping("/incomes")
  @Operation
  public ResponseEntity<List<Income>> getAllIncomes() {
    return ResponseEntity.ok(billService.getAllIncomes());
  }

  @PostMapping("/create")
  @Operation(description = "create money account")
  public ResponseEntity<ResponseMessage> createAccount(@RequestBody DtoAccount dtoAccount) {
    return ResponseEntity
        .status(HttpStatus.CREATED)
        .body(accountService.createMoneyAccount(dtoAccount));
  }

  @PutMapping("/update")
  @Operation(description = "add bill for money account")
  public ResponseEntity<ResponseMessage> updateAccount(@RequestBody DtoAccount dtoAccount) {
    return ResponseEntity
        .status(HttpStatus.CREATED)
        .body(accountService.updateMoneyAccount(dtoAccount));
  }

  @PutMapping("/cost")
  public ResponseEntity<ResponseMessage> putCost(@RequestBody DtoCost cost) {
    billService.updateAccountWithCost(cost);
    return ResponseEntity.ok().body(
        new ResponseMessage(HttpStatus.CREATED, "UPDATE SUCCESS! ADD COST!"));
  }

  @PutMapping("/income")
  public ResponseEntity<ResponseMessage> putIncome(@RequestBody DtoIncome income) {
    billService.updateAccountWithIncome(income);
    return ResponseEntity.ok().body(
        new ResponseMessage(HttpStatus.CREATED, "UPDATE SUCCESS! ADD INCOME!"));
  }

  @DeleteMapping("/delete/{id}")
  @Operation(description = "delete account")
  public ResponseEntity<ResponseMessage> deleteAccount(@PathVariable String id) {
    return ResponseEntity.ok().body(accountService.deleteAccount(id));
  }

  @DeleteMapping("/delete/income/{id}")
  @Operation(description = "delete income")
  public ResponseEntity<ResponseMessage> deleteIncome(@PathVariable String id) {
    return ResponseEntity.ok().body(billService.deleteIncome(id));
  }

  @DeleteMapping("/delete/incomes")
  @Operation(description = "delete incomes")
  public ResponseEntity<ResponseMessage> deleteAllIncomes() {
    return ResponseEntity.ok().body(billService.deleteAllIncomes());
  }

  @DeleteMapping("/delete/cost/{id}")
  @Operation(description = "delete cost")
  public ResponseEntity<ResponseMessage> deleteCost(@PathVariable String id) {
    return ResponseEntity.ok().body(billService.deleteCost(id));
  }

  @DeleteMapping("/delete/costs")
  @Operation(description = "delete incomes")
  public ResponseEntity<ResponseMessage> deleteAllCosts() {
    return ResponseEntity.ok().body(billService.deleteAllCosts());
  }
}
