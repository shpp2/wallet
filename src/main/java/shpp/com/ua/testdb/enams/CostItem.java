package shpp.com.ua.testdb.enams;

public enum CostItem {

  FOOD("food"),
  REPAIR("repair"),
  CAR("car"),
  MEDICINE("medicine"),
  CHARITY("charity"), // благодійність
  ENTERTAINMENT("entertainment"); // розваги

  CostItem(String name) {
  }

}
