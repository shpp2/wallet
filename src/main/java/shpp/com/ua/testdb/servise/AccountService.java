package shpp.com.ua.testdb.servise;

import java.util.ArrayList;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import shpp.com.ua.testdb.dto.DtoAccount;
import shpp.com.ua.testdb.dto.ResponseMessage;
import shpp.com.ua.testdb.entyties.Bill;
import shpp.com.ua.testdb.entyties.MoneyAccount;
import shpp.com.ua.testdb.repository.AccountRepository;

@Slf4j
@Service
public class AccountService {

  private final AccountRepository accountRepository;

  public AccountService(AccountRepository itemRepository) {
    this.accountRepository = itemRepository;
  }

  public List<MoneyAccount> getMoneyAccount() {
    return accountRepository.findAll();
  }

  public boolean clearDB() {
    accountRepository.deleteAll();
    return accountRepository.findAll().isEmpty();
  }

  public ResponseMessage deleteAccount(String id) {
    accountRepository.deleteById(id);
    return new ResponseMessage(HttpStatus.OK,
        "Account with ID \"" + id + "\" delete SUCCESS");
  }

  public ResponseMessage createMoneyAccount(DtoAccount dtoAccount) {
    ArrayList<Bill> bills = new ArrayList<>();
    bills.add(dtoAccount.getBill());
    if (accountRepository.findAccountByName(dtoAccount.getName()) != null) {
      return new ResponseMessage(HttpStatus.BAD_REQUEST,
          "Account was not create, because name \"" + dtoAccount.getName()
              + "\" already exist in DB!");
    }
    accountRepository.save(new MoneyAccount(dtoAccount.getName(), bills));
    return new ResponseMessage(HttpStatus.CREATED, "Account with name \""
        + dtoAccount.getName() + "\" SUCCESS created!");
  }

  public ResponseMessage updateMoneyAccount(DtoAccount dtoAccount) {
    MoneyAccount account = accountRepository.findAccountByName(dtoAccount.getName());
    // check if account is in DB
    if (account == null) {
      return new ResponseMessage(HttpStatus.BAD_REQUEST, "Account was not update, "
          + "because account with name \"" + dtoAccount.getName() + "\" not exist in DB!");
    }
    if (checkStorageUnitInBill(account, dtoAccount) == 0) {
      List<Bill> bills = account.getBills();
      bills.add(dtoAccount.getBill());
      account.setBills(bills);
      accountRepository.save(account);
      return new ResponseMessage(HttpStatus.CREATED, "Account \"" + dtoAccount.getName()
          + "\"update SUCCESS");
    } else {
      return new ResponseMessage(HttpStatus.BAD_REQUEST, "Account with name \""
          + dtoAccount.getName() + "\" already has current storage unit");
    }
  }

  private int checkStorageUnitInBill(MoneyAccount account, DtoAccount dtoAccount) {
    int counter = 0;
    for (int i = 0; i < account.getBills().size(); i++) {
      if (dtoAccount.getBill().getStorageUnit()
          .equals(account.getBills().get(i).getStorageUnit())) {
        counter++;
      }
    }
    return counter;
  }
}
