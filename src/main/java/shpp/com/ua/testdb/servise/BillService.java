package shpp.com.ua.testdb.servise;

import java.math.BigDecimal;
import java.util.List;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import shpp.com.ua.testdb.dto.DtoCost;
import shpp.com.ua.testdb.dto.DtoIncome;
import shpp.com.ua.testdb.dto.ResponseMessage;
import shpp.com.ua.testdb.enams.StorageUnit;
import shpp.com.ua.testdb.entyties.Bill;
import shpp.com.ua.testdb.entyties.Cost;
import shpp.com.ua.testdb.entyties.Income;
import shpp.com.ua.testdb.entyties.MoneyAccount;
import shpp.com.ua.testdb.repository.AccountRepository;
import shpp.com.ua.testdb.repository.CostRepository;
import shpp.com.ua.testdb.repository.IncomeRepository;

@Slf4j
@Service
public class BillService {

  private final AccountRepository accountRepository;
  private final CostRepository costRepository;
  private final IncomeRepository incomeRepository;

  public BillService(AccountRepository itemRepository, CostRepository costRepository,
      IncomeRepository incomeRepository) {
    this.accountRepository = itemRepository;
    this.costRepository = costRepository;
    this.incomeRepository = incomeRepository;
  }

  public List<Cost> getAllCosts() {
    return costRepository.findAll();
  }

  public List<Income> getAllIncomes() {
    return incomeRepository.findAll();
  }

  public void updateAccountWithCost(DtoCost cost) {
    MoneyAccount moneyAccount = accountRepository.findAccountByName(cost.getAccountName());
    Cost costs = createCost(cost);
    costRepository.save(costs);
    log.info("Save entity {} in repository success!", costs);
    StorageUnit unitCost = cost.getBill().getStorageUnit();
    for (int i = 0; i < moneyAccount.getBills().size(); i++) {
      StorageUnit unitAccount = moneyAccount.getBills().get(i).getStorageUnit();
      if (unitCost.equals(unitAccount)) {
        BigDecimal difference = calcDifference(cost.getBill().getAmount(), moneyAccount, i);
        moneyAccount.getBills().set(i, new Bill(difference, unitAccount));
        accountRepository.save(moneyAccount);
        log.info("Update account {} with cost {} in repository success!", moneyAccount.getName(),
            cost);
      }
    }
  }

  public void updateAccountWithIncome(DtoIncome income) {
    MoneyAccount moneyAccount = accountRepository.findAccountByName(income.getAccountName());
    Income incomes = createIncome(income);
    incomeRepository.save(incomes);
    log.info("Save entity {} in repository success!", incomes);
    StorageUnit unitIncome = income.getBill().getStorageUnit();
    for (int i = 0; i < moneyAccount.getBills().size(); i++) {
      StorageUnit unitAccount = moneyAccount.getBills().get(i).getStorageUnit();
      if (unitIncome.equals(unitAccount)) {
        BigDecimal amount = calcAmount(income.getBill().getAmount(), moneyAccount, i);
        moneyAccount.getBills().set(i, new Bill(amount, unitAccount));
        accountRepository.save(moneyAccount);
        log.info("Update account {} with income {} in repository success!", moneyAccount.getName(),
            income);
      }
    }
  }

  private Cost createCost(DtoCost cost) {
    return new Cost()
        .setAccountName(cost.getAccountName())
        .setCostItem(cost.getCostItem())
        .setAmount(cost.getBill().getAmount())
        .setStorageUnit(cost.getBill().getStorageUnit());
  }

  private BigDecimal calcDifference(BigDecimal subtrahend, MoneyAccount moneyAccount, int i) {
    BigDecimal reduced = moneyAccount.getBills().get(i).getAmount();
    return reduced.subtract(subtrahend);
  }

  private Income createIncome(DtoIncome income) {
    return new Income()
        .setAccountName(income.getAccountName())
        .setIncomeItem(income.getIncomeItem())
        .setStorageUnit(income.getBill().getStorageUnit())
        .setAmount(income.getBill().getAmount());
  }

  private BigDecimal calcAmount(BigDecimal addition, MoneyAccount moneyAccount, int i) {
    BigDecimal accountSum = moneyAccount.getBills().get(i).getAmount();
    return accountSum.add(addition);
  }

  public ResponseMessage deleteIncome(String id) {
    DtoIncome dtoIncome = createDtoIncome(incomeRepository.findById(id));
    MoneyAccount moneyAccount = accountRepository.findAccountByName(dtoIncome.getAccountName());
    accountRepository.save(updateMoneyAccountBeforeDelIncome(moneyAccount, dtoIncome));
    log.info("Update account {} in repository success!", moneyAccount.getName());
    incomeRepository.deleteById(id);
    log.info("Delete income with id: {} SUCCESS", id);
    return new ResponseMessage(HttpStatus.OK,
        "Delete income with id: \"" + id + "\" SUCCESS");
  }

  private DtoIncome createDtoIncome(Income income) {
    return new DtoIncome(income.getAccountName(), income.getIncomeItem(),
        new Bill(income.getAmount(), income.getStorageUnit()));
  }

  private MoneyAccount updateMoneyAccountBeforeDelIncome(MoneyAccount moneyAccount,
      DtoIncome dtoIncome) {
    StorageUnit unitIncome = dtoIncome.getBill().getStorageUnit();
    for (int i = 0; i < moneyAccount.getBills().size(); i++) {
      StorageUnit unitAccount = moneyAccount.getBills().get(i).getStorageUnit();
      if (unitIncome.equals(unitAccount)) {
        BigDecimal difference = calcDifference(dtoIncome.getBill().getAmount(), moneyAccount, i);
        moneyAccount.getBills().set(i, new Bill(difference, unitAccount));
      }
    }
    return moneyAccount;
  }

  public ResponseMessage deleteAllIncomes() {
    incomeRepository.deleteAll();
    return new ResponseMessage(HttpStatus.OK, "DB with incomes CLEAR!");
  }

  public ResponseMessage deleteCost(String id) {
    DtoCost dtoCost = createDtoCost(costRepository.findById(id));
    MoneyAccount moneyAccount = accountRepository.findAccountByName(dtoCost.getAccountName());
    accountRepository.save(updateMoneyAccountBeforeDelIncome(moneyAccount, dtoCost));
    log.info("Update account {} in repository success!", moneyAccount.getName());
    costRepository.deleteById(id);
    log.info("Delete income with id: {} SUCCESS", id);
    return new ResponseMessage(HttpStatus.OK, "Cost with id: " + id + " delete SUCCESS!");
  }

  private MoneyAccount updateMoneyAccountBeforeDelIncome(MoneyAccount moneyAccount,
      DtoCost dtoCost) {
    StorageUnit unitIncome = dtoCost.getBill().getStorageUnit();
    for (int i = 0; i < moneyAccount.getBills().size(); i++) {
      StorageUnit unitAccount = moneyAccount.getBills().get(i).getStorageUnit();
      if (unitIncome.equals(unitAccount)) {
        BigDecimal amount = calcAmount(dtoCost.getBill().getAmount(), moneyAccount, i);
        moneyAccount.getBills().set(i, new Bill(amount, unitAccount));
      }
    }
    return moneyAccount;
  }

  private DtoCost createDtoCost(Cost cost) {
    return new DtoCost(cost.getAccountName(), cost.getCostItem(),
        new Bill(cost.getAmount(), cost.getStorageUnit()));
  }

  public ResponseMessage deleteAllCosts() {
    costRepository.deleteAll();
    return new ResponseMessage(HttpStatus.OK, "DB with costs CLEAR!");
  }

}
