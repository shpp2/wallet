package shpp.com.ua.testdb.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import shpp.com.ua.testdb.entyties.Bill;

@Getter
@Setter
@ToString
public class DtoAccount {
  @JsonProperty(value = "name")
  String name;
  @JsonProperty(value = "bill", defaultValue = "UAH")
  Bill bill;
}
