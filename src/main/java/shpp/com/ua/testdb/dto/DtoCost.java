package shpp.com.ua.testdb.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import shpp.com.ua.testdb.enams.CostItem;
import shpp.com.ua.testdb.entyties.Bill;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class DtoCost {

  @JsonProperty(value = "account_name")
  private String accountName;
  @JsonProperty(value = "cost_item")
  private CostItem costItem;
  @JsonProperty(value = "bill")
  private Bill bill;

}
