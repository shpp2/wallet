package shpp.com.ua.testdb.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.v3.oas.annotations.media.Schema;
import java.time.LocalDateTime;
import lombok.Getter;
import org.springframework.http.HttpStatus;

@Getter
@Schema(name = "Schema ExceptionMessage",
    description = "ExceptionMessage is a transfer object for inform the frontend developer")
public class ResponseMessage {

  @JsonProperty(value = "current_time")
  private LocalDateTime currentTime = LocalDateTime.now();
  @JsonProperty(value = "http_status")
  private HttpStatus httpStatus;
  @JsonProperty(value = "message")
  private String message;

  public ResponseMessage(HttpStatus httpStatus, String message) {
    this.httpStatus = httpStatus;
    this.message = message;
  }
}
