package shpp.com.ua.testdb.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;
import shpp.com.ua.testdb.enams.IncomeItem;
import shpp.com.ua.testdb.entyties.Bill;

@Getter
@Setter
@ToString
@AllArgsConstructor
public class DtoIncome {

  @JsonProperty(value = "account_name")
  private String accountName;
  @JsonProperty(value = "income_item")
  private IncomeItem incomeItem;
  @JsonProperty(value = "bill")
  private Bill bill;

}
