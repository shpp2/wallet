package shpp.com.ua.testdb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import shpp.com.ua.testdb.entyties.MoneyAccount;

public interface AccountRepository extends MongoRepository<MoneyAccount, Integer> {

  MoneyAccount findAccountByName(String name);

  void deleteById(String id);

}
