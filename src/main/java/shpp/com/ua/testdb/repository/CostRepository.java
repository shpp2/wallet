package shpp.com.ua.testdb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import shpp.com.ua.testdb.entyties.Cost;

public interface CostRepository extends MongoRepository<Cost, Integer> {

  void deleteById(String id);

  Cost findById(String id);
}
