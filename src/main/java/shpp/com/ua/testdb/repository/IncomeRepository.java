package shpp.com.ua.testdb.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import shpp.com.ua.testdb.entyties.Income;

public interface IncomeRepository extends MongoRepository<Income, Integer> {

  void deleteById(String id);

  Income findById(String id);
}
